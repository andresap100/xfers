package service;

public class OrderedLock {

	public static <T> void lockAndRun(T o1, T o2, Runnable op) {
		T first = o1;
		T second = o2;
		//to prevent deadlock we are locking accounts in the same order
		if (System.identityHashCode(first) > System.identityHashCode(second)) {
			first = o2;
			second = o1;
		}
		synchronized (first) {
			synchronized (second) {
				op.run();
			}
		}
	}

}
