package service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class Storage<T, K> {
	private Map<K, T> objects = new ConcurrentHashMap<>();

	public void create(T obj, K id) {
		objects.put(id, obj);
	}

	public T putIfAbsent(T obj, K id) {
		return objects.putIfAbsent(id, obj);
	}

	public Optional<T> find(K id) {
		return Optional.ofNullable(objects.get(id));
	}

}
