package service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import bank.Account;
import bank.AccountOperation;
import bank.Bank;
import bank.Transfer;
import bank.TransferStatus;
import exceptions.DuplicateRequestException;
import exceptions.NotFoundException;
import exceptions.ValidationException;

public class BankService implements Bank {
	private Storage<Transfer, String> txns = new Storage<>();
	private Storage<Account, Integer> accs = new Storage<>();
	private AtomicInteger ids = new AtomicInteger();

	@Override
	public Account createAccount(Account a) {
		a.setId(ids.incrementAndGet());
		accs.create(a, a.getId());
		return a;
	}

	@Override
	public Account getAccount(int id) {
		return accs.find(id).orElseThrow(() -> new NotFoundException("Account " + id + " not found"));
	}

	@Override
	public Transfer getTransfer(String externalId) {
		return txns.find(externalId).orElseThrow(() -> new NotFoundException("Transfer " + externalId + " not found"));
	}

	@Override
	public List<AccountOperation> getOperations(int id) {
		return getAccount(id).getOperations();
	}

	@Override
	public Transfer transfer(Transfer t) {
		if ((t.getExternalId() == null) || t.getExternalId().isEmpty()) {
			throw new ValidationException("External id should be present");
		}
		if (t.getAmountPence() <= 0) {
			throw new ValidationException("Amount should be positive");
		}
		if (t.getFrom() == t.getTo()) {
			throw new ValidationException("Accounts should be different");
		}

		Account from = getAccount(t.getFrom());
		Account to = getAccount(t.getTo());

		Transfer existing = txns.putIfAbsent(t, t.getExternalId());
		if (existing != null) {
			throw new DuplicateRequestException();
		}
		performTtransfer(t, from, to);
		return t;
	}

	protected void performTtransfer(Transfer t, Account from, Account to) {
		t.setStatus(TransferStatus.REQUESTED);
		try {
			OrderedLock.lockAndRun(from, to, () -> from.transferFunds(to, t.getAmountPence()));
			t.setStatus(TransferStatus.COMPLETED);
		} catch (RuntimeException ex) {
			t.setStatus(TransferStatus.FAILED);
			throw ex;
		}
	}

}
