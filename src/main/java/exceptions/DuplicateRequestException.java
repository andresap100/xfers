package exceptions;

public class DuplicateRequestException extends RuntimeException {

	public DuplicateRequestException() {
		super("Duplicate request detected");

	}

}
