package rest;
import bank.Account;
import bank.AccountOperation;
import bank.Bank;
import bank.Transfer;
import exceptions.DuplicateRequestException;
import exceptions.InsufficientBalanceException;
import exceptions.NotFoundException;
import exceptions.ValidationException;
import io.jooby.Jooby;
import io.jooby.StatusCode;
import io.jooby.json.JacksonModule;
import service.BankService;

public class BankServer extends Jooby {
	private static final String PREFIX = "/api/";
	public static final String TRANSFERS_PATH = PREFIX + "transfers";
	public static final String ACCOUNTS_PATH = PREFIX + "accounts";
	public static final String OPERATIONS = "operations";

	public BankServer(Bank bank) {
		install(new JacksonModule());
		errors();
		post(TRANSFERS_PATH, ctx -> bank.transfer(ctx.body(Transfer.class)));
		get(TRANSFERS_PATH + "/{id}", ctx -> bank.getTransfer(ctx.path("id").toString()));
		post(ACCOUNTS_PATH, ctx -> bank.createAccount(ctx.body(Account.class)));
		get(ACCOUNTS_PATH + "/{id}", ctx -> bank.getAccount(ctx.path("id").intValue()));
		get(ACCOUNTS_PATH + "/{id}/" + OPERATIONS, ctx -> bank.getAccount(ctx.path("id").intValue()).getOperations().toArray(new AccountOperation[]{}));

	}

	private void errors() {
		errorCode(DuplicateRequestException.class, StatusCode.CONFLICT);
		errorCode(NotFoundException.class, StatusCode.NOT_FOUND);
		errorCode(ValidationException.class, StatusCode.BAD_REQUEST);
		errorCode(InsufficientBalanceException.class, StatusCode.BAD_REQUEST);
	}

	public static void main(String[] args) {
		runApp(args, () -> new BankServer(new BankService()));
	}

}
