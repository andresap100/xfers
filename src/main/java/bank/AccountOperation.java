package bank;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class AccountOperation {
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss z")
	private Date date;
	private int amountInPence;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getAmountInPence() {
		return amountInPence;
	}

	public void setAmountInPence(int amountInPence) {
		this.amountInPence = amountInPence;
	}

	public AccountOperation(Date date, int amountInPence) {
		this.date = date;
		this.amountInPence = amountInPence;
	}

	public AccountOperation() {
	}

}
