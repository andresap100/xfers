package bank;

import java.util.List;

public interface Bank {
	Transfer transfer(Transfer t);

	Transfer getTransfer(String externalId);

	Account createAccount(Account account);

	Account getAccount(int id);

	List<AccountOperation> getOperations(int id);
}
