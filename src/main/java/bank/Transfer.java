package bank;

public class Transfer {
	private int from;
	private int to;
	private int amountPence;
	private String externalId;

	private TransferStatus status = TransferStatus.REQUESTED;

	public Transfer() {
		super();
	}

	public Transfer(int from, int to, int amountPence, String externalId) {
		super();
		this.from = from;
		this.to = to;
		this.amountPence = amountPence;
		this.externalId = externalId;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public int getAmountPence() {
		return amountPence;
	}

	public void setAmountPence(int amountPence) {
		this.amountPence = amountPence;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public TransferStatus getStatus() {
		return status;
	}

	public void setStatus(TransferStatus status) {
		this.status = status;
	}

}
