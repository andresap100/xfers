package bank;

public enum TransferStatus {
	REQUESTED, COMPLETED, FAILED;
}
