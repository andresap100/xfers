package bank;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import exceptions.InsufficientBalanceException;

public class Account {
	private int balancePence;
	private int id;

	@JsonIgnore
	private List<AccountOperation> operations = new ArrayList<>();

	public Account() {
	}

	public Account(int id, int balancePence) {
		this.balancePence = balancePence;
		this.id = id;
	}

	public void transferFunds(Account to, int amount) {
		if (getBalancePence() < amount) {
			throw new InsufficientBalanceException("Insufficient balance on account " + getId() + " : " + getBalancePence() + ". Requested " + amount);
		}
		setBalancePence(getBalancePence() - amount);
		to.setBalancePence(to.getBalancePence() + amount);
		Date date = new Date();
		getOperations().add(new AccountOperation(date, -amount));
		to.getOperations().add(new AccountOperation(date, amount));
	}

	public int getBalancePence() {
		return balancePence;
	}

	public void setBalancePence(int balancePence) {
		this.balancePence = balancePence;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<AccountOperation> getOperations() {
		return operations;
	}

}
