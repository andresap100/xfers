package rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static service.Verifications.AMOUNT;
import static service.Verifications.B1;
import static service.Verifications.B2;
import static service.Verifications.uniq;

import org.junit.Test;

import bank.Account;
import bank.Transfer;
import bank.TransferStatus;
import exceptions.DuplicateRequestException;
import rest.client.BankClient;
import service.Verifications;

public class TransferServerTest {

	@Test
	public void test() throws Exception {
		LockableServer server = new LockableServer();
		server.startRunAndStop(() -> {
			BankClient client = server.newClient();
			checkRequestInProgress(server, client);

			new Verifications(client);
			return null;
		});
	}

	private static void checkRequestInProgress(LockableServer server, BankClient bank) throws Exception {
		int a1 = bank.createAccount(new Account(0, B1)).getId();
		int a2 = bank.createAccount(new Account(0, B2)).getId();

		assertEquals(B1, bank.getAccount(a1).getBalancePence());
		assertEquals(B2, bank.getAccount(a2).getBalancePence());

		Transfer t = new Transfer(a1, a2, AMOUNT, uniq());

		server.getLocked().add(a1);
		Thread thread = new Thread(() -> server.newClient().transfer(t));
		thread.start();
		server.getTransferStartedLatch().await();

		assertEquals(TransferStatus.REQUESTED, bank.getTransfer(t.getExternalId()).getStatus());

		try {
			bank.transfer(t);
			fail();
		} catch (DuplicateRequestException ex) {
			//expected
		}

		checkTransferForOtherAccountsIsWorking(bank);

		server.getPreventTransferLatch().countDown();
		thread.join();
		assertEquals(TransferStatus.COMPLETED, bank.getTransfer(t.getExternalId()).getStatus());

		assertEquals(B1 - AMOUNT, bank.getAccount(a1).getBalancePence());
		assertEquals(B2 + AMOUNT, bank.getAccount(a2).getBalancePence());

	}

	private static void checkTransferForOtherAccountsIsWorking(BankClient bank) {

		int a3 = bank.createAccount(new Account(0, B1)).getId();
		int a4 = bank.createAccount(new Account(0, B1)).getId();

		assertEquals(B1, bank.getAccount(a3).getBalancePence());

		bank.transfer(new Transfer(a3, a4, AMOUNT, uniq()));

		assertEquals(B1 - AMOUNT, bank.getAccount(a3).getBalancePence());
		assertEquals(B1 + AMOUNT, bank.getAccount(a4).getBalancePence());
	}

}
