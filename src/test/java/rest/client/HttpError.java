package rest.client;

public class HttpError extends RuntimeException {
	private final int code;

	public HttpError(int code, String msg) {
		super(msg);
		this.code = code;
	}

	public int getCode() {
		return code;
	}

}
