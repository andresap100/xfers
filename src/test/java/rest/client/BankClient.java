package rest.client;

import java.util.Arrays;
import java.util.List;

import bank.Account;
import bank.AccountOperation;
import bank.Bank;
import bank.Transfer;
import exceptions.DuplicateRequestException;
import exceptions.InsufficientBalanceException;
import exceptions.NotFoundException;
import exceptions.ValidationException;
import io.jooby.StatusCode;
import rest.BankServer;

public class BankClient implements Bank {
	private String serverUrl;
	private HttpDriver http = new HttpDriver();

	public BankClient(String serverUrl) {
		super();
		this.serverUrl = serverUrl;
	}

	@Override
	public Transfer transfer(Transfer t) {
		try {
			return http.post(Transfer.class, t, serverUrl + BankServer.TRANSFERS_PATH);
		} catch (HttpError ex) {
			if (ex.getCode() == StatusCode.NOT_FOUND_CODE) {
				throw new NotFoundException(ex.getMessage());
			}
			if (ex.getCode() == StatusCode.CONFLICT_CODE) {
				throw new DuplicateRequestException();
			}
			if (ex.getCode() == StatusCode.BAD_REQUEST_CODE) {
				if (ex.getMessage().contains("sufficient")) {
					throw new InsufficientBalanceException(ex.getMessage());
				} else {
					throw new ValidationException(ex.getMessage());
				}
			}
			throw ex;
		}
	}

	@Override
	public Transfer getTransfer(String externalId) {
		try {
			return http.get(Transfer.class, serverUrl + BankServer.TRANSFERS_PATH + "/" + externalId);
		} catch (HttpError ex) {
			if (ex.getCode() == StatusCode.NOT_FOUND_CODE) {
				throw new NotFoundException(ex.getMessage());
			}
			throw ex;
		}
	}

	@Override
	public Account createAccount(Account account) {
		return http.post(Account.class, account, serverUrl + BankServer.ACCOUNTS_PATH);
	}

	@Override
	public Account getAccount(int id) {
		try {
			return http.get(Account.class, serverUrl + BankServer.ACCOUNTS_PATH + "/" + id);
		} catch (HttpError ex) {
			if (ex.getCode() == StatusCode.NOT_FOUND_CODE) {
				throw new NotFoundException(ex.getMessage());
			}
			throw ex;
		}
	}

	@Override
	public List<AccountOperation> getOperations(int id) {
		try {
			return Arrays.asList(http.get(AccountOperation[].class, serverUrl + BankServer.ACCOUNTS_PATH + "/" + id + "/" + BankServer.OPERATIONS));
		} catch (HttpError ex) {
			if (ex.getCode() == StatusCode.NOT_FOUND_CODE) {
				throw new NotFoundException(ex.getMessage());
			}
			throw ex;
		}
	}

}
