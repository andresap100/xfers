package rest.client;

import java.io.IOException;
import java.util.concurrent.Callable;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jooby.StatusCode;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpDriver {
	private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
	private static ObjectMapper objectMapper = new ObjectMapper();

	public <T> T post(Class<T> clazz, T obj, String url) {
		return process(() -> {
			RequestBody body = RequestBody.Companion.create(objectMapper.writeValueAsString(obj), JSON);
			Request request = new Request.Builder().url(url).post(body).build();
			return request(clazz, request);
		});
	}

	public <T> T get(Class<T> clazz, String url) {
		return process(() -> request(clazz, new Request.Builder().url(url).build()));
	}

	private static <T> T process(Callable<T> callable) {
		try {
			return callable.call();
		} catch (HttpError ex) {
			throw ex;
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private static <T> T request(Class<T> clazz, Request request) throws IOException {
		return objectMapper.readValue(request(request), clazz);
	}

	private static String request(Request request) throws IOException {
		try (Response response = new OkHttpClient().newCall(request).execute()) {
			String text = response.body().string();
			if (response.code() == StatusCode.OK_CODE) {
				return text;
			} else {
				throw new HttpError(response.code(), text);
			}
		}
	}

}
