package rest;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import bank.Account;
import bank.Bank;
import bank.Transfer;
import io.jooby.Jooby;
import io.jooby.ServerOptions;
import rest.client.BankClient;
import service.BankService;

public class LockableServer {
	private int port;
	private CountDownLatch preventTransferLatch = new CountDownLatch(1);
	private CountDownLatch transferStartedLatch = new CountDownLatch(1);
	private Set<Integer> locked = new HashSet<>();

	public void startRunAndStop(Callable<String> callable) throws Exception {
		this.port = findRandomOpenPort();
		BankServer server = startServer(port, lockableBank());
		try {
			callable.call();
		} finally {
			server.stop();
		}
	}

	private Bank lockableBank() {
		return new BankService() {
			@Override
			public void performTtransfer(Transfer t, Account from, Account to) {
				lock(from.getId());
				super.performTtransfer(t, from, to);
			}

		};
	}

	private void lock(int fromId) {
		if (locked.contains(fromId)) {
			try {
				transferStartedLatch.countDown();
				preventTransferLatch.await();
			} catch (Exception ex) {
				//ignore
			}
		}
	}

	public static BankServer startServer(int port, Bank bank) {
		BankServer server = new BankServer(bank);
		ServerOptions serverOptions = new ServerOptions();
		serverOptions.setPort(port);
		server.setServerOptions(serverOptions);
		new Thread(() -> Jooby.runApp(new String[]{}, () -> server)).start();
		return server;
	}

	public static int findRandomOpenPort() throws IOException {
		try (ServerSocket socket = new ServerSocket(0);) {
			return socket.getLocalPort();
		}
	}

	public BankClient newClient() {
		return new BankClient("http://localhost:" + port);
	}

	public CountDownLatch getPreventTransferLatch() {
		return preventTransferLatch;
	}

	public CountDownLatch getTransferStartedLatch() {
		return transferStartedLatch;
	}

	public Set<Integer> getLocked() {
		return locked;
	}

}
