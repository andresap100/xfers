package service;

import java.util.concurrent.CountDownLatch;

import org.junit.Test;
/**
 * checks that locking works correctly for concurrent mirror transfers.
 * */
public class OrderedLockTest {

	@Test(timeout = 5000)
	public void test() throws InterruptedException {
		//number of attempts is required to achieve deadlock
		for (int i = 0; i < 1000; i++) {
			checkDeadlock();
		}
	}

	private void checkDeadlock() throws InterruptedException {
		CountDownLatch latch1 = new CountDownLatch(1);
		CountDownLatch latch2 = new CountDownLatch(1);

		Thread ta = new Thread(() -> OrderedLock.lockAndRun(latch1, latch2, () -> await(latch1)));
		Thread tb = new Thread(() -> OrderedLock.lockAndRun(latch2, latch1, () -> await(latch2)));

		ta.start();
		tb.start();

		latch1.countDown();
		latch2.countDown();

		ta.join();
		tb.join();
	}

	private static void await(CountDownLatch l) {
		try {
			l.await();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
