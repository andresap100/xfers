package service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.UUID;

import bank.Account;
import bank.Bank;
import bank.Transfer;
import bank.TransferStatus;
import exceptions.DuplicateRequestException;
import exceptions.InsufficientBalanceException;
import exceptions.NotFoundException;
import exceptions.ValidationException;

public class Verifications {

	public static final int B1 = 1000;
	public static final int B2 = 2 * B1;
	public static final int AMOUNT = B1 / 10;

	public static final int NON_EXISTING_ACCOUNT = 999;
	public static final String NON_EXISTING_XFER = "NON_EXISTING_XFER";

	private Bank bank;
	private int a1;
	private int a2;

	public Verifications(Bank bank) {
		super();
		this.bank = bank;
		a1 = bank.createAccount(new Account(0, B1)).getId();
		a2 = bank.createAccount(new Account(0, B2)).getId();
		assertBalances(B1, B2);
		transferAndDuplicate();
		insufficientBalance();
		externalIdNull();
		missingExternalId();
		nonExistingAccountFrom();
		nonExistingAccountTo();
		nonExistingTransfer();
		getNonExistingAccount();
		zeroAmount();
		negativeAmount();
		sameAccount();
		fullBalance();
	}

	private void transferAndDuplicate() {
		assertBalances(B1, B2);

		Transfer t = bank.transfer(new Transfer(a1, a2, AMOUNT, uniq()));
		assertEquals(TransferStatus.COMPLETED, t.getStatus());
		assertEquals(TransferStatus.COMPLETED, bank.getTransfer(t.getExternalId()).getStatus());
		assertBalances(B1 - AMOUNT, B2 + AMOUNT);

		assertEquals(1, bank.getOperations(a1).size());
		assertEquals(-AMOUNT, bank.getOperations(a1).get(0).getAmountInPence());

		assertEquals(1, bank.getOperations(a2).size());
		assertEquals(AMOUNT, bank.getOperations(a2).get(0).getAmountInPence());

		bank.transfer(new Transfer(a2, a1, AMOUNT, uniq()));
		assertBalances(B1, B2);

		try {
			bank.transfer(t);
			fail();
		} catch (DuplicateRequestException ex) {
			//expected
		}
		assertBalances(B1, B2);
	}

	private void insufficientBalance() {
		assertBalances(B1, B2);
		Transfer t = new Transfer(a1, a2, B1 + 1, uniq());
		try {
			bank.transfer(t);
			fail();
		} catch (InsufficientBalanceException ex) {
			assertTrue(ex.getMessage().contains("Insufficient"));
		}
		assertEquals(TransferStatus.FAILED, bank.getTransfer(t.getExternalId()).getStatus());

		assertBalances(B1, B2);
	}

	private void externalIdNull() {
		try {
			bank.transfer(new Transfer(a1, a2, AMOUNT, null));
			fail();
		} catch (ValidationException ex) {
			assertTrue(ex.getMessage().contains("External id should be present"));
		}
	}

	private void missingExternalId() {
		try {
			bank.transfer(new Transfer(a1, a2, AMOUNT, ""));
			fail();
		} catch (ValidationException ex) {
			assertTrue(ex.getMessage().contains("External id should be present"));
		}
	}

	private void nonExistingAccountTo() {
		String externalId = uniq();
		try {
			bank.transfer(new Transfer(NON_EXISTING_ACCOUNT, a2, AMOUNT, externalId));
			fail();
		} catch (NotFoundException ex) {
			assertTrue(ex.getMessage().contains("Account " + NON_EXISTING_ACCOUNT + " not found"));
		}

		checkTransferNotFound(externalId);
	}

	private void nonExistingAccountFrom() {
		String externalId = uniq();
		try {
			bank.transfer(new Transfer(a1, NON_EXISTING_ACCOUNT, AMOUNT, externalId));
			fail();
		} catch (NotFoundException ex) {
			assertTrue(ex.getMessage().contains("Account " + NON_EXISTING_ACCOUNT + " not found"));
		}
		checkTransferNotFound(externalId);
	}

	private void nonExistingTransfer() {
		checkTransferNotFound(NON_EXISTING_XFER);
	}

	private void checkTransferNotFound(String externalId) {
		try {
			bank.getTransfer(externalId);
			fail();
		} catch (NotFoundException ex) {
			assertTrue(ex.getMessage().contains(externalId));
		}
	}

	private void zeroAmount() {
		assertBalances(B1, B2);
		Transfer t = new Transfer(a1, a2, 0, uniq());
		try {
			bank.transfer(t);
			fail();
		} catch (ValidationException ex) {
			assertTrue(ex.getMessage().contains("Amount should be positive"));
		}
		checkTransferNotFound(t.getExternalId());
		assertBalances(B1, B2);
	}

	private void negativeAmount() {
		assertBalances(B1, B2);
		Transfer t = new Transfer(a1, a2, -AMOUNT, uniq());
		try {
			bank.transfer(t);
			fail();
		} catch (ValidationException ex) {
			assertTrue(ex.getMessage().contains("Amount should be positive"));
		}
		checkTransferNotFound(t.getExternalId());
		assertBalances(B1, B2);
	}

	private void sameAccount() {
		assertBalances(B1, B2);
		Transfer t = new Transfer(a1, a1, AMOUNT, uniq());
		try {
			bank.transfer(t);
		} catch (ValidationException ex) {
			assertTrue(ex.getMessage().contains("Accounts should be different"));
		}
		checkTransferNotFound(t.getExternalId());

		assertBalances(B1, B2);
	}

	private void fullBalance() {
		assertBalances(B1, B2);

		bank.transfer(new Transfer(a1, a2, B1, uniq()));
		assertBalances(0, B1 + B2);

		bank.transfer(new Transfer(a2, a1, B1, uniq()));
		assertBalances(B1, B2);
	}

	private void getNonExistingAccount() {
		try {
			bank.getAccount(NON_EXISTING_ACCOUNT);
			fail();
		} catch (NotFoundException e) {
			assertTrue(e.getMessage().contains("" + NON_EXISTING_ACCOUNT));
		}
	}

	public static String uniq() {
		return UUID.randomUUID().toString();
	}

	private void assertBalances(int b1, int b2) {
		assertEquals(b1, bank.getAccount(a1).getBalancePence());
		assertEquals(b2, bank.getAccount(a2).getBalancePence());
	}

}
