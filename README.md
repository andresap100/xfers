# Build

The project had been built using Java 8, Gradle 5.4.1, Jooby.

In order to build and run server on port 8080 type: 

```bash
gradle run
```

In order to create distribution zip in `build/distributions` type: 

```bash
gradle distZip
```

#API overview
```
  POST /api/transfers
  GET  /api/transfers/{id}
  POST /api/accounts
  GET  /api/accounts/{id}
  GET  /api/accounts/{id}/operations
```

# Create Transfer

New transfer can be created by sending POST request to `api/transfers` with transfer attributes encoded in JSON. Client should generate new unique *externalId* for a transfer, e.g. UUID. 
For example, following JSON can be used to transfer one pound from account 10 to account 11 :

```js
{"from":10, "to": 11, "amountPence":100, "externalId":"1234"}
```


```bash
curl --header "Content-Type: application/json" --request POST --data '{"from":1, "to": 2, "amountPence":100, "externalId":"1234"}'  http://localhost:8080/api/transfers
```

Transfer JSON will be returned as a result. Status field will contain one of the following:

*  COMPLETED - transfer had completed successfully
*  FAILED - transfer failed, e.g. "from" account balance is too low.
*  REQUESTED - if processing is still ongoing

If given *externalId* had been received already, operation will fail and status code will be 409 Conflict.


Result JSON: 

```js
{
	"from": 1,
	"to": 2,
	"amountPence": 100,
	"externalId": "1234",
	"status": "COMPLETED"
}
```


# Get transfer

GET request to `api/transfers/{externalId}`     

```bash
curl --request GET  http://localhost:8080/api/transfers/abc123d
```

Result JSON:

```js
{"from":2,"to":1,"amountPence":15,"externalId":"abc123d","status":"COMPLETED"}
```


# Create Account

You can create new account by sending POST request to `api/accounts`. The initial balance should be sent as JSON - `{"balancePence":100}`.  

```bash
curl --header "Content-Type: application/json" --request POST --data '{"balancePence":100}'  http://localhost:8080/api/accounts
```
  
Result JSON:

```js
{"balancePence":100,"id":1}
```
  
  
# Get account

GET request to `api/accounts/{id}`     

```bash
curl --request GET  http://localhost:8080/api/accounts/1
```

Result JSON:

```js
{"balancePence":100,"id":1}
```


# Get account operations

GET request to `/api/accounts/{id}/operations`     

```bash
curl --request GET  http://localhost:8080/api/accounts/1/operations
```

Result JSON:

```js
[{"date":"2019-08-10 01:37:01 UTC","amountInPence":-100},{"date":"2019-08-10 01:37:01 UTC","amountInPence":15}]
```

 
